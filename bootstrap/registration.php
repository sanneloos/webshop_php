<?php
include_once __DIR__ . '/utilities/validation.php';
include_once __DIR__ . '/utilities/formHelpers.php';

$validationResults = array();
$formHasErrors = false;
    
if (isset($_POST['register'])) {
    // De gebruiker heeft de login button geklikt.
    // Valideer de gegevens
    $validationResults['email'] = validateEmail($_POST['email']);
    $validationResults['password'] = validatePassword($_POST['password']);
    $validationResults['confirmed-password'] = validatePassword($_POST['confirmed-password'], true);

    // Het paswoord veld en het bevestigt paswoord horen hetzelfde te zijn.
    if ($_POST['password'] !== $_POST['confirmed-password']) {
        $validationResults['error']['confirmed-password'] = 'The passwords don\'t match.';
    }
    
    $validationResults['firstname'] = validateString($_POST['firstname'], 'je voornaam');
    $validationResults['name'] = validateString($_POST['name'], 'je naam');
    $validationResults['street'] = validateString($_POST['street'], 'je adres');
    $validationResults['zip'] = validateString($_POST['zip'], 'je postcode');
    $validationResults['city'] = validateString($_POST['city'], 'je gemeente / stad');
    $validationResults['country'] = validateString($_POST['country'], 'je land');
    $validationResults['phone'] = validateString($_POST['phone'], 'je telefoonnummer');
    
    foreach($validationResults as $result) {
        if (isset($result['error'])) {
            $formHasErrors = true;
            break;
        }
    }
    
    if ($formHasErrors === false) {
        // Sla de waarden op in de database.
        header('Location: home.php');
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bootstrap, from Twitter</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .registration {
                max-width: 350px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .registration .registration-heading {
                margin-bottom: 10px;
            }
            .registration input[type="text"],
            .registration input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

            .registration label.error {
                color: red;
            }
        </style>
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="assets/ico/favicon.png">
    </head>

    <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="home.php">Webshop</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
                <li><a href="home.php">Home</a></li>
                <li><a href="index.php">Login</a></li>
                <li class="active"><a href="registration.php">Registratie</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <br/>

        <div class="container">

            <form class="registration" action="registration.php" method="post">
                <h2 class="registration-heading">Registratie</h2>

                 <?php echo getErrorLabel($validationResults, 'firstname'); ?>
                <input type="text" class="input-block-level" name="firstname" id="street" placeholder="Voornaam" value="<?php echo getValue($validationResults, 'firstname'); ?>">
                
                <?php echo getErrorLabel($validationResults, 'name'); ?>
                <input type="text" class="input-block-level" name="name" id="street" placeholder="Naam" value="<?php echo getValue($validationResults, 'name'); ?>">
                
                <?php echo getErrorLabel($validationResults, 'email'); ?>
                <input type="text" class="input-block-level" name="email" id="email" placeholder="Email adres" value="<?php echo getValue($validationResults, 'email'); ?>">

                <?php echo getErrorLabel($validationResults, 'password'); ?>
                <input type="password" class="input-block-level" name="password" id="password" placeholder="Paswoord">

                <?php echo getErrorLabel($validationResults, 'confirmed-password'); ?>
                <input type="password" class="input-block-level" name="confirmed-password" id="confirmed-password" placeholder="Geef je paswoord nogmaals in ter bevestiging">
                
                <?php echo getErrorLabel($validationResults, 'street'); ?>
                <input type="text" class="input-block-level" name="street" id="street" placeholder="Straat + nr." value="<?php echo getValue($validationResults, 'street'); ?>">

                <?php echo getErrorLabel($validationResults, 'zip'); ?>
                <input type="text" class="input-block-level" name="zip" id="zip" placeholder="Postcode" value="<?php echo getValue($validationResults, 'zip'); ?>">

                <?php echo getErrorLabel($validationResults, 'city'); ?>
                <input type="text" class="input-block-level" name="city" id="city" placeholder="Gemeente / Stad" value="<?php echo getValue($validationResults, 'city'); ?>">

                <?php echo getErrorLabel($validationResults, 'country'); ?>
                <input type="text" class="input-block-level" name="country" id="country" placeholder="Land" value="<?php echo getValue($validationResults, 'country'); ?>">

                <?php echo getErrorLabel($validationResults, 'phone'); ?>
                <input type="text" class="input-block-level" name="phone" id="phone" placeholder="Telefoonnummer" value="<?php echo getValue($validationResults, 'phone'); ?>">

                <label class="checkbox">
                    <input type="checkbox" name="agree" id="remember-me" value="agree"> Ik ga akkoord met de <a href="#">Terms & Services</a>
                </label>
                
                <br/>
                
                <button class="btn btn-large btn-primary" name="register" id="register" type="submit">Registreer</button>
                
                
            </form>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap-transition.js"></script>
        <script src="assets/js/bootstrap-alert.js"></script>
        <script src="assets/js/bootstrap-modal.js"></script>
        <script src="assets/js/bootstrap-dropdown.js"></script>
        <script src="assets/js/bootstrap-scrollspy.js"></script>
        <script src="assets/js/bootstrap-tab.js"></script>
        <script src="assets/js/bootstrap-tooltip.js"></script>
        <script src="assets/js/bootstrap-popover.js"></script>
        <script src="assets/js/bootstrap-button.js"></script>
        <script src="assets/js/bootstrap-collapse.js"></script>
        <script src="assets/js/bootstrap-carousel.js"></script>
        <script src="assets/js/bootstrap-typeahead.js"></script>

    </body>
</html>
