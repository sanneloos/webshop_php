<?php

namespace Model;

class Address {

    private $street;
    private $number;
    private $postcode;
    private $city;
    private $land;

    public function getStreet() {
        return $this->street;
    }

    public function setStreet($value) {
        $this->street = $value;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($value) {
        $this->number = $value;
    }

    public function getPostcode() {
        return $this->postcode;
    }

    public function setPostcode($value) {
        $this->postcode = $value;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($value) {
        $this->city = $value;
    }

    public function getLand() {
        return $this->land;
    }

    public function setLand($value) {
        $this->land = $value;
    }
}

?>
