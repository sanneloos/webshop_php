<?php
namespace Model; 

class User {
    private $lastname;
    private $firstname;
    private $username;
    private $password;
    private $address;
    private $telephone;
    private $email;
    private $facturation_address;
    private $delivery_address;
    private $preferencePaySystems;
    private $interestedInNewsletter;
    private $registrationDone;
    private $shoppingCart;
    private $orders;
    private $favorites;
    private $isBlocked;
    
    public function getLastName()
    {
        return $this->lastname;
    }
    
    public function setLastName($value)
    {
        $this->lastname = $value;
    }
    
    public function getFirstName()
    {
        return $this->firstname;
    }
    
    public function setFirstName($value)
    {
        $this->firstname = $value;
    }
    
    public function getUserName()
    {
        return $this->username;
    }
    
    public function setUserName($value)
    {
        $this->username = $value;
    }
    
    public function getPassword()
    {
        return $this->password;
    }
    
    public function setPassword($value)
    {
        $this->password = $value;
    }
    
    public function getAddress()
    {
        return $this->address;
    }
    
    public function setAddress($value)
    {
        $this->address = $value;
    }
    
    public function getTelephone()
    {
        return $this->telephone;
    }
    
    public function setTelephone($value)
    {
        $this->telephone = $value;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($value)
    {
        $this->email = $value;
    }
    
    public function getFacturationAddress()
    {
        return $this->facturation_address;
    }
    
    public function setFacturationAddress($value)
    {
        $this->facturation_address = $value;
    }
    
    public function getDeliveryAddress()
    {
        return $this->delivery_address;
    }
    
    public function setDeliveryAddress($value)
    {
        $this->delivery_address = $value;
    }
    
    public function getPreferencePaySystems()
    {
        return $this->preferencePaySystems;
    }
    
    public function setPreferencePaySystems($value)
    {
        $this->preferencePaySystems = $value;
    }
    
    public function getInterestedInNewsletter()
    {
        return $this->interestedInNewsletter;
    }
    
    public function setInterestedInNewsletter($value)
    {
        $this->interestedInNewsletter = $value;
    }
    
    public function getRegistrationDone()
    {
        return $this->registrationDone;
    }
    
    public function setRegistrationDone($value)
    {
        $this->registrationDone = $value;
    }
    
    public function getShoppingCart()
    {
        return $this->shoppingCart;
    }
    
    public function setShoppingCart($value)
    {
        $this->shoppingCart = $value;
    }
    
    public function getOrders()
    {
        return $this->orders;
    }
    
    public function setOrders($value)
    {
        $this->orders = $value;
    }
    
    public function getFavorites()
    {
        return $this->favorites;
    }
    
    public function setFavorites($value)
    {
        $this->favorites = $value;
    }
    
    public function getIsBlocked()
    {
        return $this->isBlocked;
    }
    
    public function setIsBlocked($value)
    {
        $this->isBlocked = $value;
    }
    
    public function Register()
    {
        
    }
    
    public function Login()
    {
        
    }
    
    public function Logout()
    {
        
    }
}

?>
