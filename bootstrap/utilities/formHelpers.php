<?php

function getErrorLabel(array $validationResults, $field)
{
    $label = '';
    if (isset($validationResults[$field]['error'])) {
        $label = '<label class="error" for="' . $field . '">'
                . $validationResults[$field]['error']
                . '</label>';
    }

    return $label;
}

function getValue(array $validationResults, $field)
{
    $value = '';
    if (isset($validationResults[$field]['value'])) {
        $value = $validationResults[$field]['value'];
    }

    return $value;
}

?>
