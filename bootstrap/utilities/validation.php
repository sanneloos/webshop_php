<?php

function validateEmail($email)
{
    $result = array();
    $error = null;

    if ($email != '') {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Geef aub een geldig email adres in:';
        }
    } else {
        $error = 'Geef aub je email adres in:';
    }
    
    $result['value'] = $email;
    $result['error'] = $error;
    
    return $result;
}

function validatePassword($password, $passwordConfirmation=false)
{
    $result = array();
    $error = null;

    if ($password != '') {
        $password = filter_var($password, FILTER_SANITIZE_STRING);

        if ($password == '') {
            $error = 'Geef aub een geldig paswoord in:';
        }
    } else {
        // Begin met de default waarde
        $error = 'Geef aub je paswoord in:';

        if ($passwordConfirmation == true) {
            // Wijs de uitzondering toe.
            $error = 'Geef aub je paswoord in ter bevestiging:';
        }
    }

    $result['value'] = $password;
    $result['error'] = $error;
    
    return $result;
}

function validateString($string, $label)
{
    $result = array();
    $error = null;

    if ($string != '') {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
    } else {
        $error = "Geef aub $label in:";
    }

    $result['value'] = $string;
    $result['error'] = $error;
    
    return $result;
}

?>
