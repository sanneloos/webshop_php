<?php
include_once __DIR__ . DIRECTORY_SEPARATOR . 'autoload.php';

$errors = false;

if (isset($_POST['login'])) {
// De gebruiker heeft de login button geklikt.
    if ($_POST['email'] != '') {
        $_POST['email'] = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Geef aub een geldige emailadres in.<br/><br/>";
        }
    } else {
        $errors['email'] = "Geef aub uw emailadres in.<br/><br/>";
    }

    if ($_POST['password'] != '') {
        $_POST['password'] = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

        if ($_POST['password'] == '') {
            $errors['password'] = "Geef aub een geldige wachtwoord in (geen spaties).<br/><br/>";
        }
    } else {
        $errors['password'] = "Geef aub uw wachtwoord in.<br/><br/>";
    }

    if ($errors === false) {
// Sla de waarden op in de database.
        /* header('Location: home.php'); */

//set cookies voor users en pass voor een jaar
        if (isset($_POST['rememberMe'])) {
            $time = time() + 60 * 60 * 24 * 365;
            setcookie('username', $_POST['email'], $time);
            setcookie('password', md5($_POST['password']), $time);
        } else {
            // cookie expires when browser closes
            setcookie('username', $_POST['email'], false);
            setcookie('password', md5($_POST['password']), false);
        }
//then redirect them to the members area
        header('Location: home.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Webshop | Inloggen</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin, .register {
                max-width: 350px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox,
            .register .register-heading {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

            .form-signin label.error {
                color: red;
            }
        </style>
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="assets/ico/favicon.png">
    </head>

    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="home.php">Webshop</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="home.php">Home</a></li>
                            <li class="active"><a href="index.php">Login</a></li>
                            <li><a href="registration.php">Registratie</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <br/>

        <div class="container">

            <form class="form-signin" action="index.php" method="post">
                <h2 class="form-signin-heading">Log aub in</h2>
                <input type="text" class="input-block-level" name="email" id="email" placeholder="Email adres" >
                <?php if (isset($errors['email'])) { ?>
                    <label class="error" for="email"><?php echo $errors['email']; ?></label>
                <?php } ?>
                <input type="password" class="input-block-level" name="password" id="password" placeholder="Wachtwoord">
                <?php if (isset($errors['password'])) { ?>
                    <label class="error" for="password"><?php echo $errors['password']; ?></label>
                <?php } ?>

                <label class="checkbox">
                    <input type="checkbox" name="rememberMe" id="remember-me" value="1"> Bewaar mijn gegevens
                </label>

                <button class="btn btn-primary" name="login" id="login" type="submit">Inloggen</button>
            </form>

            <div class="register">
                <h2 class="register-heading">Nieuw op onze webshop?</h2>
                <a href="registration.php" class="btn btn-primary"s>Registreer nu</a>
            </div>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap-transition.js"></script>
        <script src="assets/js/bootstrap-alert.js"></script>
        <script src="assets/js/bootstrap-modal.js"></script>
        <script src="assets/js/bootstrap-dropdown.js"></script>
        <script src="assets/js/bootstrap-scrollspy.js"></script>
        <script src="assets/js/bootstrap-tab.js"></script>
        <script src="assets/js/bootstrap-tooltip.js"></script>
        <script src="assets/js/bootstrap-popover.js"></script>
        <script src="assets/js/bootstrap-button.js"></script>
        <script src="assets/js/bootstrap-collapse.js"></script>
        <script src="assets/js/bootstrap-carousel.js"></script>
        <script src="assets/js/bootstrap-typeahead.js"></script>

    </body>
</html>
